<?php

echo "<H1> MAKE AN INITIAL USER WITH THIS THEN DELETE IT</h1>";

function getForm(string $submitName, string $submitValue)
{
    $form = <<<HEREDOC
    <form method="POST">
    <label for="username">User Name : </label>
    <input type="text" name="username" id="username" required>
    <label for="password">Password : </label>
    <input type="text" name="password" id="password" required>
    <input type="submit" name="$submitName" value="$submitValue">
    </form>
HEREDOC;
    return $form;
}
// build forms - UNCOMMENT THE BELOW TWO LINES TO GET THIS WORKING
//$userForm = getForm('submit_user', 'Add User');
//$loginForm = getForm('submit_login', 'Login');

/* add a new user to flat file database */
echo $userForm;
if (isset($_POST['submit_user'])) {
    // retrieve user input - you still need to do data validation and sanitizing
    $userName = (isset($_POST['username'])) ? $_POST['username'] : null;
    $passWord = (isset($_POST['password'])) ? $_POST['password'] : null;
    $passWord = password_hash($passWord, PASSWORD_DEFAULT); // store a hash
    // get user.json file
    $file = "./users.json";
    $users = json_decode(file_get_contents($file), true);
    // insert new user credentials
    $users['users'][] = ['name' => $userName, 'password' => $passWord];
    // write  to flat file database
    file_put_contents($file, json_encode($users));
}
/* login - verify user credentials */
echo $loginForm;
if (isset($_POST['submit_login'])) {
    // retrieve user input - you still need to do data validation and sanitizing
    $userName = (isset($_POST['username'])) ? $_POST['username'] : null;
    $passWord = (isset($_POST['password'])) ? $_POST['password'] : null;

    // get user.json file
    $file = "./users.json";
    $users = json_decode(file_get_contents($file), true);

    // verify user
    foreach ($users['users'] as $key => $value) {
        if (strtolower($value['name']) === strtolower($userName)) {
            $hash = $value['password'];
            $verify = password_verify($passWord, $hash); // verify
            if ($verify === true) {
                echo 'User Login Validated';
            } else echo 'Login Not Valid';
        }
    }
}
