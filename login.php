<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


session_start();


function getForm(string $submitName, string $submitValue)
{
    $form = <<<HEREDOC
    <form method="POST">
    <label for="username">User Name : </label>
    <input type="text" name="username" id="username" required>
    <label for="password">Password : </label>
    <input type="password" name="password" id="password" required>
    <input type="submit" name="$submitName" value="$submitValue">
    </form>
HEREDOC;
    return $form;
}


// 

	


if (count($_POST) > 0) {   // we've been here - either GOOD or BAD login has happened
	echo "nope";
	if (isset($_POST['submit_login'])) {
		
		// retrieve user input - you still need to do data validation and sanitizing
		$userName = (isset($_POST['username'])) ? $_POST['username'] : null;
		$passWord = (isset($_POST['password'])) ? $_POST['password'] : null;

		// get user.json file
		$file = "./users.json";
		$users = json_decode(file_get_contents($file), true);
		
		if (empty($users)) { echo "<br>passfile error, have you run the first time thing?";}

		// verify user
		foreach ($users['users'] as $key => $value) {
			if (strtolower($value['name']) === strtolower($userName)) {
				$hash = $value['password'];
				$verify = password_verify($passWord, $hash); // verify
				if ($verify === true) {
					echo '<br>User Login Validated';
							$_SESSION['is_logged_in'] = true;
							header("Location: admin.php");
					
				} else  { 
					echo '<br>Login Not Valid, try again?';
					$loginForm = getForm('submit_login', 'Login');
					echo $loginForm;
			}
		}
	} 
	
}

} else {  // first time, present login form
	
$loginForm = getForm('submit_login', 'Login');
echo $loginForm;
	
}


