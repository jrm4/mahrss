
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN">
<head>
	<title>Mah RSS</title>

<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"> 

<?php
# Hey, lets put the file variables here:
#simplepieloader = $_SERVER['DOCUMENT_ROOT'] . '/mahrss/simplepie-1.5/autoloader.php';

require_once 'vendor/autoload.php';

#COLOR PICKER
function genColorCodeFromText($text)
{

$numberstring = crc32($text);
 
$hue = $numberstring % 360;
$saturation = $numberstring % 40 + 10;
$lightness = $numberstring % 25 + 65; 
    return "hsl($hue, $saturation%, $lightness%)";
    
}

function genColorCodeFromText2($text)
{

$numberstring = crc32($text);
 
$hue = $numberstring % 360;
$saturation = $numberstring % 40;
$lightness = $numberstring % 25 + 55; 
    return "hsl($hue, $saturation%, $lightness%)";
    
}



if (isset($_GET['rssfile'])) {
	$cssbgcolor = genColorCodeFromText($_GET['rssfile']);
} else {
	$cssbgcolor = "white";
}

$css2 = genColorCodeFromText2($_GET['rssfile']);
?>


<!--  THE MAIN CSS - the majority of the styles for the app are here, in fact, I believe the only one that is not is for adding the
color of the categories for the dropdown, which took some hackery -->


<style>

	body {
		font-family: 'Cabin','Lato', Fallback, sans-serif;
		color:Black;
		background-color:<?php echo $cssbgcolor ?>;
		/* width:700px; */
		margin:50px auto;
		padding:10px;
	}
 
	a {
		text-decoration:none;
		color:Black;
		padding:0 1px
	}
 
	a:hover {
		background-color:#333;
		coloir:#fff;
		text-decoration:none;
	}
 

	div.header {
		background-color: Black;
		color:<?php echo $cssbgcolor ?>;
		height: 75px;
		text-align: left;
		margin-bottom: 20px;
		margin-right: 4px;
	}
 
	div.item {
		/* font-size:90%; */
		color:White;
		padding-bottom: 10px;
		padding-top: 5px;
		border-spacing:1px; /* this is for firefox android i think? */
		margin-bottom:1px;
		margin-right:4px;
		padding-left:20px;
		font-size:120%;
	}
	
div.item:nth-of-type(2n+1) {
	background-color: <?php echo $css2 ?>;
}

div.block {
	margin-bottom:40px;
}

/* favicons are the only images (for now) */

	img {
		height: 30px;
		padding: 10px;
		}






</style>


</head>
<body>


<!-- and now, the content.  SIMPLEPIE IS APPARENTLY DEPRECATED and I'm supposed to be using Picofeed. Again, later --> 



<?php




// Given a file, parses the feeds out line by line, then feeds the feeds into SimplePie and prints them out. //
function getfeed($feedfile) {

	$feedArray = explode("\n", file_get_contents($feedfile));


	foreach ($feedArray as $feedurl){
		$feed = new SimplePie();
		$feed->set_cache_location("./cache");
		$feed->set_feed_url($feedurl);
		$feed->init();
		$feed->handle_content_type();

/*------ FIRST THE TITLE STUFF ----------------*/		

		$fpermalink = $feed->get_permalink();
		$ftitle = $feed->get_title();
		$fdesc = $feed->get_description();

/*---- simplepie's favicon grabber is broken. stupidly trying to write my own. I think google can do one too, but no.  ---*/

	$urlData = parse_url($feedurl);
	$host = $urlData['host'];
	
	$ficon = "http://" . $host . "/favicon.ico";
	
		
		
/* ---- here is the text, echo it RIGHT AFTER -- This is HEREDOC!!! */ 
$titlethang =<<<TITLETHANG

	<div class="block"> 
	<div class="header">



		<h1>
		<a style="color:inherit;" href=$fpermalink>$ftitle</a>

		<img src="$ficon" ALIGN=RIGHT></h1>
	</div>

TITLETHANG;


echo $titlethang;
	

/*-- now for the feed items */

	foreach($feed->get_items() as $item){

		$itemlink = $item->get_permalink();
		$itemtitlepre = $item->get_title();

		$itemtitle=htmlspecialchars_decode($itemtitlepre);

                $itemthang =<<<ITEMTHANG

                        <div class="item">
                        <a href=$itemlink>$itemtitle</a>
                        </div>
ITEMTHANG;

                echo $itemthang;
	}

	echo "</div>";
	}

}


/* ----------  generate dropdown from files, right? 
 * Note that I've also called that genColorCode function again here, to color in the background of the dropdown
 *
 * (which tells us how crappy the dropdown colors currently are
 *
 *
 * ---- */



function rssmenu() { 

	echo '<form method="get" action="index.php">';
	echo '<select name="rssfile">';

	$rssfiles = glob("./rss/*");

	foreach ($rssfiles as $file) {
		$basefile = basename($file);
		$bgcolor = genColorCodeFromText($file);				
		echo "<option ";
                if ($_GET['rssfile'] == $file){echo "selected ";} 
		echo "value=$file ";
			echo "style=\"background-color: $bgcolor\" >$basefile";
			echo "</option>";
                        echo "\r\n";

	}
		echo '</select> <input type="submit" name="submit" value="Select" />';
		echo '</form>';
}


# function addfeedform ($currentfeedfile) {
#	echo '<form method="post" action="addfeed.php">';
#	echo 'Add feed: <input name="feed" type="text" />';
#	echo ' to ';
#	$basefile = basename($currentfeedfile);
#	echo '<input name="feedfile" value="';
#	echo $basefile;
#	echo '">';
#
#	echo '<input type="submit">';
#}
// --- FUNNY ENOUGH, the CODE DOESN'T ACTUALLY BEGIN RUNNING UNTIL RIGHT NOW ---


// Make the dropdown menu
rssmenu();



// If we've selected an rss file, GREAT, gimme them feeds. If not, CHOOSE SOME FEEDS, MAN.
if (isset($_GET['rssfile'])) { 
	getfeed($_GET['rssfile']);
	addfeedform($_GET['rssfile']);
} else {
echo "choose above, yo";
}


echo "<br><br><a style=\"color:blue\" href=\"./login.php\">admin page</a>";

?>
